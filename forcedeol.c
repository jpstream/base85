/*
 * Copyright 2019-2021 Steinar Knutsen
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence. You may
 * obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * Licence for the specific language governing permissions and limitations under
 * the Licence.
 */

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>

int main(int argc, char *argv[])
{
    int c;
    int lastoutput = -1;
    int linelen = 0;
    while ((c = getchar()) != EOF) {
        if (putchar(c) == EOF) {
            return EXIT_FAILURE;
        }
        lastoutput = c;
        if (lastoutput == '\n') {
            linelen = 0;
        } else {
            ++linelen;
        }
        if (linelen >= 80) {
            if (putchar('\n') == EOF) {
                return EXIT_FAILURE;
            }
            lastoutput = '\n';
            linelen = 0;
        }
    }
    if (ferror(stdin)) {
        return EXIT_FAILURE;
    }
    if (lastoutput != '\n') {
        if (putchar('\n') == EOF) {
            return EXIT_FAILURE;
        }
    }
    return EXIT_SUCCESS;
}
